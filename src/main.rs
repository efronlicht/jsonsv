#[cfg(test)]
mod tests;
use {
    jsonschema_valid::validate,
    serde_json::Value,
    std::{
        fs::File,
        io::{Read, Write},
        process::exit,
    },
    structopt::StructOpt,
};

fn main() {
    let Opt {
        schema,
        instance,
        verbose,
        ..
    } = Opt::from_args();
    if verbose {
        dbg!(&schema, &instance);
    }
    match (
        from_file_or_str(&schema, verbose),
        from_file_or_str(&instance, verbose),
    ) {
        (Ok(schema), Ok(instance)) => match is_valid(&schema, &instance) {
            Ok(()) => {
                println!("valid");
                exit(0)
            }
            Err(e) => {
                println!("invalid");
                eprintln!("{}", e);
            }
        },
        (Ok(_), Err(i_err)) => eprintln!(
            "error: loading instance from '{}': invalid json: {}",
            instance, i_err
        ),
        (Err(s_err), Ok(_)) => eprintln!(
            "error: loading schema from '{}', invalid json {}",
            schema, s_err
        ),
        (Err(s_err), Err(i_err)) => eprintln!(
            "error loading schema: invalid json: {}\nerror: loading instance as json: {}",
            s_err, i_err
        ),
    }
    exit(1)
}
pub fn is_valid(schema: &Value, instance: &Value) -> Result<(), String> {
    let errs = validate(&instance, &schema, None, false);
    let v = errs.get_errors();
    if v.is_empty() {
        Ok(())
    } else {
        let mut buf = Vec::new();
        for e in v {
            writeln!(buf, "{}", e).unwrap();
        }
        Err(String::from_utf8(buf).unwrap())
    }
}

#[derive(Debug, Clone, StructOpt)]
/// jsonsv validates an instance of JSON against a provided schema.
/// it prints "valid" or "invalid" to stdout, and validation errors, if any, to stderr
struct Opt {
    #[structopt()]
    /// the schema to validate against. tries to load the path given as a file. if that file doesn't exist, treats the input as a string.
    schema: String,

    /// the instance to validate with the schema. tries to load the path given as a file. if that file dosn't exist, treats the input as as string.
    #[structopt()]
    instance: String,

    /// chattier output to stderr
    #[structopt(short = "d", long = "verbose")]
    verbose: bool,
}

pub fn from_file_or_str(mut s: &str, verbose: bool) -> serde_json::Result<Value> {
    s = s.trim();
    match std::path::Path::new(s).canonicalize() {
        Ok(path) => {
            if verbose {
                eprintln!("loading from file: {}", path.to_string_lossy())
            }
            let mut buf = String::new();
            match File::open(&path).and_then(|mut f| f.read_to_string(&mut buf)) {
                Ok(_) => serde_json::from_str(&buf),
                Err(err) => Err(serde_json::Error::io(err)),
            }
        }
        Err(_) => {
            if verbose {
                eprintln!("attempting to load from string: \"{}\"", s)
            }
            serde_json::from_str(s)
        }
    }
}
